import React, { useState } from "react";
import {
  BrowserRouter,
  Routes,
  Route,
  Link,
  useLocation,
} from "react-router-dom";

import icon from "./icon.svg";
import docker from "./docker.svg";
import cloud from "./cloud.svg";
import terminal from "./terminal.svg";
import fire from "./fire.svg";
import gear from "./gear.svg";

import screenshot from "./app-screenshot.png";
import endpointScreenShot from "./endpoint.png";
import smartAppBuilderScreenshot from "./smart-app-builder2.png";
import dndFeatureScreenshot from "./dnd-feature.png";
import deploymentFeatureScreenshot from "./deployment.png";
import terminologyScreenshot from "./terminology.png";
import fhirpathFeatureScreenshot from "./fhirpath-editor.png";
import fhirpathFeatureScreenshot2 from "./fhirpath-editor2.png";

import TermsOfService from "./terms-of-service";
import PrivacyPolicy from "./privacy-policy";
// import video from "./app-video.mov";
import "./App.css";

function HeaderContent() {
  return (
    <div className="mt-8 md:flex space-y-8 md:space-y-0 text-white">
      <div className="md:w-1/2 lg:w-1/2 flex-none flex flex-col items-center md:items-start lg:pt-4">
        <h2 className="text-3xl font-bold mt-2 text-center ">
          Create modern healthcare apps fast.
        </h2>
        <p className="mt-2 text-center sm:text-left">
          Create, Deploy and test FHIR based applications.
          <br />
        </p>
        <div className="mt-4 flex flex-col sm:flex-row items-center space-y-2 sm:space-y-0 sm:space-x-2">
          <span>Build with FHIR:</span>
          <div className="flex items-center justify-center sm:justify-start space-x-2">
            <a target="_blank" href="https://hl7.org/fhirpath/">
              <span className="rounded-full bg-blue-100 text-orange-700 hover:bg-white hover:text-orange-800  px-3 py-1.5 text-xs ">
                FHIRPath
              </span>
            </a>
            <a
              target="_blank"
              href="https://www.hl7.org/fhir/operationdefinition.html"
            >
              <span className="rounded-full bg-blue-100 text-orange-700 hover:bg-white hover:text-orange-800 px-3 py-1.5 text-xs ">
                OperationDefinitions
              </span>
            </a>
            <a target="_blank" href="https://www.hl7.org/fhir/endpoint.html">
              <span className="rounded-full bg-blue-100 text-orange-700 hover:bg-white hover:text-orange-800 px-3 py-1.5 text-xs ">
                Endpoints
              </span>
            </a>
          </div>
        </div>
        <div className="mt-16 flex items-center space-x-3">
          <div className="inline-flex rounded-md shadow">
            <a
              href="https://workspace.genfhi.app"
              target="_blank"
              className="font-bold inline-flex items-center justify-center space-x-2 px-4 py-2 border border-transparent rounded-md text-white bg-orange-600 hover:bg-orange-50 hover:text-orange-700"
            >
              <span>Sign up for Cloud Workspace</span>
            </a>
          </div>
          <div className="inline-flex">
            <a
              href="https://gitlab.com/genfhi/genfhi/container_registry/2417387"
              className="inline-flex items-center justify-center space-x-2 px-4 py-2 border border-transparent rounded-md text-orange-700 bg-blue-100 hover:bg-white"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
                aria-hidden="true"
                className="w-5 h-5"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M4 16v1a3 3 0 003 3h10a3 3 0 003-3v-1m-4-4l-4 4m0 0l-4-4m4 4V4"
                ></path>
              </svg>
              <span>Download image</span>
            </a>
          </div>
        </div>
        <p className="mt-4 text-xs text-center sm:text-left text-white">
          Use a managed cloud environment or self-deploy and run your own
          workspace.
        </p>
      </div>
      <div className="flex-1 min-w-0 flex items-center justify-end ">
        <iframe
          width="100%"
          height="100%"
          src="https://www.youtube.com/embed/u57P4hz_csE"
          title="YouTube video player"
          frameborder="0"
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
          allowfullscreen
        ></iframe>
      </div>
    </div>
  );
}

function Section({ style, className, children }) {
  return (
    <div
      style={style}
      className={`-skew-y-3 p-20 overflow-x-hidden font-semibold ${className}`}
    >
      <div className={`mx-auto py-4 sm:pb-8 relative skew-y-3`}>{children}</div>
    </div>
  );
}

function Menu({ children }) {
  const [showMobileMenu, setShowMobileMenu] = useState(false);
  return (
    <header id="home">
      <Section style={{ marginTop: "-75px" }} className="bg-blue-500">
        <div className="flex items-center justify-between">
          <a className="flex items-center space-x-4" href="/">
            <img src={icon} className="w-12 h-12" />
            <h1 className="text-3xl font-bold text-white">GenFHI</h1>
          </a>
          <nav className="hidden md:flex flex-wrap justify-center space-x-2 font-bold">
            <a
              target="_blank"
              className="px-4 py-2 rounded-md   text-white hover:text-gray-200 "
              href="https://docs.genfhi.app"
            >
              Docs
            </a>
            <a
              href="https://gitlab.com/genfhi/genfhi"
              target="_blank"
              className="px-4 py-2 rounded-md  text-white hover:text-gray-200 "
            >
              Source
            </a>
            <a
              href="mailto:business@genfhi.app"
              className="px-4 py-2 rounded-md   text-white hover:text-gray-200 "
            >
              Contact
            </a>
            <a
              className="px-4 py-2 rounded-md hover:bg-orange-50 hover:text-orange-700 bg-orange-600  text-slate-50 "
              href="https://workspace.genfhi.app"
              target="_blank"
            >
              Sign in
            </a>
          </nav>
          <div className="block md:hidden relative z-20">
            <button
              onClick={() => setShowMobileMenu(!showMobileMenu)}
              className="p-2 rounded-md hover:bg-orange-100"
              id="headlessui-popover-button-1"
              type="button"
              aria-expanded="false"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
                aria-hidden="true"
                className="w-5 h-5 text-gray-700"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="3"
                  d="M4 6h16M4 12h16M4 18h16"
                ></path>
              </svg>
            </button>
            {showMobileMenu && (
              <div>
                <div className="absolute right-0 bg-white w-64 p-2 rounded-md shadow-lg">
                  <nav className="flex flex-col space-y-1 font-bold">
                    <a
                      className="p-2 rounded-lg hover:bg-orange-50  text-gray-600 hover:text-gray-900 "
                      href="/"
                    >
                      Home
                    </a>
                    <a
                      className="p-2 rounded-lg hover:bg-orange-50  text-gray-600 hover:text-gray-900 "
                      href="/#features"
                    >
                      Features
                    </a>
                    <a
                      href="https://gitlab.com/genfhi/genfhi"
                      target="_blank"
                      className="p-2 rounded-lg hover:bg-orange-50  text-gray-600 hover:text-gray-900 "
                    >
                      Source
                    </a>

                    <a
                      href="mailto:business@genfhi.app"
                      target="_blank"
                      className="p-2 rounded-lg hover:bg-orange-50  text-gray-600 hover:text-gray-900 "
                    >
                      Contact
                    </a>
                  </nav>
                </div>
              </div>
            )}
          </div>
        </div>
        {children}
      </Section>
    </header>
  );
}

function ContentSection({ title, left, right }) {
  return (
    <div
      id="features"
      className="container lg:mt-12 mt-8 mx-auto grid grid-cols-1 lg:grid-cols-2"
    >
      <div className="mb-24 mr-2">
        <div>
          <h3 className="text-3xl m-0 p-0 font-bold">{title}</h3>
        </div>
        {left}
      </div>
      <div>{right}</div>
    </div>
  );
}

function ImageCard({ style, title, image, className }) {
  return (
    <div
      style={style}
      className={`${
        className || ""
      } bg-orange-200 text-orange-900 rounded-lg overflow-hidden border relative drop-shadow-lg`}
    >
      {title && <div className="font-bold p-4 py-2">{title}</div>}
      <img src={image} />
    </div>
  );
}

function Home() {
  return (
    <div className="mx-auto">
      <Section className="text-blue-900">
        <ContentSection
          title="Build on top of open healthcare standards"
          left={
            <div>
              <p className="mt-4 mb-12">
                Create applications based on the most popular open standards in
                healthcare.
              </p>
              <div className="grid grid-cols-1 md:grid-cols-3">
                <div className="mb-8 mr-4">
                  <div>
                    <div
                      style={{ borderLeftWidth: "3px" }}
                      className="border-l-4 border-blue-900 pl-2 mb-2 text-xl font-bold"
                    >
                      FHIR
                    </div>
                    <p className="text-xs">
                      We have built GenFHI from the ground up around the open
                      FHIR standard. This allows you to have access to data from
                      your EHR and/or any FHIR based server, to seamlessly
                      launch applications within your EHR and to be able to
                      utilize shared logic and/or terminologies from third
                      parties.
                    </p>
                  </div>
                </div>
                <div className="mb-8 mr-4">
                  <div
                    style={{ borderLeftWidth: "3px" }}
                    className="border-blue-900 border-l-4 pl-2 mb-2 text-xl font-bold "
                  >
                    Ease of use
                  </div>
                  <p className="text-xs">
                    FHIR is a powerful standard but it is also difficult to jump
                    into as a developer. To that end we've focused on making it
                    easy for developers to explore and build on top of FHIR.
                    This includes auto completion against your FHIR servers
                    capabilities. A powerful FHIRPath editor for users that
                    displays results, autocompletion on variables and has
                    fhirpath syntax. And an intuitive drag and drop interface
                    around app building and terminology editing.
                  </p>
                </div>
                <div className="mb-8 mr-4">
                  <div
                    style={{ borderLeftWidth: "3px" }}
                    className="border-l-4 border-blue-900 pl-2 mb-2 text-xl font-bold"
                  >
                    Interoperability
                  </div>
                  <p className="text-xs">
                    We are capable of interoperating with most EHRs and any
                    server based around the FHIR standard. Additionally we are
                    able to automatically query your EHR and/or FHIR server to
                    determine it's capabilities and show those capabilities as
                    you build your apps.
                  </p>
                </div>
              </div>
            </div>
          }
          right={
            <div className="min-w-0 flex items-center justify-center">
              <ImageCard
                style={{ top: "-50px", left: "40%" }}
                image={endpointScreenShot}
                title={"Endpoint Editor"}
                className="md:w-4/5 max-w-sm shrink-0 lg:w-3/5 border-orange-400"
              />
              <ImageCard
                image={smartAppBuilderScreenshot}
                title={"SMART on FHIR App Builder"}
                className="z-10 md:w-4/5 max-w-sm shrink-0 lg:w-3/5 border-orange-400"
              />
              <ImageCard
                style={{ top: "-80px", left: "-40%" }}
                image={terminologyScreenshot}
                title={"Terminology Editor"}
                className="md:w-4/5 max-w-sm shrink-0 lg:w-3/5 border-orange-400"
              />
            </div>
          }
        />
      </Section>
      <Section className="bg-blue-900 text-white">
        <ContentSection
          title="Deployment"
          right={
            <ImageCard
              className="border-blue-900"
              image={deploymentFeatureScreenshot}
            />
          }
          left={
            <div>
              <p className="mt-4 mb-4 ">
                GenFHI supports numerous options for deploying your application
                into production.
              </p>
              <div className="grid grid-cols-1 md:grid-cols-3 mt-12 ">
                <div className="mb-8 mr-4">
                  <img src={cloud} className="w-12 h-12 mb-2" />
                  <div
                    style={{ borderLeftWidth: "3px" }}
                    className="border-l-4 border-white pl-2 mb-2 text-xl font-bold "
                  >
                    Cloud
                  </div>
                  <p className="text-xs">
                    We can host your application on our servers. Choosing this
                    option will provide you with a shareable link. Additionally
                    if you want a private link on your subnet we can deploy
                    GenFHI as a kubernetes cluster in your subnet.
                  </p>
                </div>
                <div className="mb-8 mr-4">
                  <div>
                    <img src={docker} className="w-12 h-12 mb-2" />
                    <div
                      style={{ borderLeftWidth: "3px" }}
                      className="border-l-4 border-white pl-2 text-xl font-bold mb-2"
                    >
                      Docker
                    </div>
                    <p className="text-xs">
                      We can generate a single docker image for your app. After
                      downloading you can run your image on-premise or on top of
                      a cloud provider (AWS, GCP, Azure).
                    </p>
                  </div>
                </div>
                <div className="mb-8 mr-4">
                  <div>
                    <img src={terminal} className="w-10 h-12 mb-2" />
                    <div
                      style={{ borderLeftWidth: "3px" }}
                      className="border-l-4 border-white pl-2 text-xl font-bold mb-2"
                    >
                      File system
                    </div>
                    <p className="text-xs">
                      You can download your application as a json file and run
                      it using GenFHIs app runner image. This allows you to run
                      multiple applications from a single process.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          }
        />
      </Section>
    </div>
  );
}

function MainMenu() {
  const location = useLocation();
  let Child = () => <div />;
  if (location.pathname === "/") Child = HeaderContent;

  return (
    <Menu>
      <Child />
    </Menu>
  );
}

function App() {
  return (
    <>
      <BrowserRouter>
        <MainMenu />
        <Routes>
          <Route path="/">
            <Route index element={<Home />} />
            <Route path="privacy" element={<PrivacyPolicy />} />
            <Route path="terms" element={<TermsOfService />} />
          </Route>
        </Routes>
        <footer className="mt-12 lg:w-3/4 mx-auto pb-4">
          <nav className="flex items-center justify-center">
            <div className="px-4 py-2">
              <a href="/#home" className="text-gray-500 hover:text-gray-900">
                Home
              </a>
            </div>

            <div className="px-4 py-2">
              <a
                href="https://gitlab.com/genfhi/genfhi/container_registry/2417387"
                className="text-gray-500 hover:text-gray-900"
              >
                Download Image
              </a>
            </div>
            <div className="px-4 py-2">
              <Link
                to="/privacy"
                onClick={(e) => {
                  // Scroll back to top.
                  window.scrollTo(0, 0);
                }}
                className="text-gray-500 hover:text-gray-900"
              >
                Privacy Policy
              </Link>
            </div>
            <div className="px-4 py-2">
              <Link
                to="/terms"
                className="text-gray-500 hover:text-gray-900"
                onClick={(e) => {
                  // Scroll back to top.
                  window.scrollTo(0, 0);
                }}
              >
                Terms of Service
              </Link>
            </div>
          </nav>
        </footer>
      </BrowserRouter>
    </>
  );
}

export default App;
